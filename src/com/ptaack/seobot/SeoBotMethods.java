package com.ptaack.seobot;


import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

import org.apache.commons.httpclient.ConnectMethod;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.ProxyClient;
import org.apache.commons.httpclient.ProxyClient.ConnectResponse;
import org.apache.commons.httpclient.methods.GetMethod;

import com.ptaack.seobot.model.Proxy;
import com.ptaack.seobot.proxylist.AnonymousProxy;

public class SeoBotMethods {

	public static void fetchAnonymProxy() {
		AnonymousProxy anonymProxy = new AnonymousProxy();
		Proxy proxy;
		for(String p: anonymProxy.getList()) {
			proxy = new Proxy(p);
			if (!SeoBotDatabase.isDublicate(proxy)) {
				SeoBotDatabase.addAnonymProxy(proxy);
			}
		}
	}

	public static void fakeVisit(String site) {
		HttpClient client = new HttpClient();
		GetMethod method = new GetMethod();
		
		Proxy proxy = (Proxy) SeoBotDatabase.getRandomObject("anonymproxy");
		
		Main.log("Selected proxy: " + proxy.toString());
		
		HostConfiguration hostConf = client.getHostConfiguration();
		
		hostConf.setHost(site);
		hostConf.setProxy(proxy.getIp(), proxy.getPort());
		
		//client.getParams().setSoTimeout(30 * 1000);
		method.getParams().setParameter("http.useragent",SeoBotDatabase.getRandomObject("useragent"));
		
		try {
			client.executeMethod(method);
		} catch (Exception e) {
			System.err.println("Proxy is dead");
			SeoBotDatabase.markAsDead(proxy);
		} finally {
			method.releaseConnection();
		}
		
//		Socket socket = null;
//		
//		try {
//			ConnectResponse response = client.connect();
//			//System.out.println(response.getConnectMethod().getResponseBodyAsString());
//			socket = response.getSocket();
//			if(socket == null) {
//				ConnectMethod method = response.getConnectMethod();
//				System.err.println("Socket not created: " + method.getStatusLine());
//			}
//			// do something
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			if(socket != null) 
//				try { 
//					socket.close(); 
//				} catch (Exception fe) {}
//		}
	}

}
