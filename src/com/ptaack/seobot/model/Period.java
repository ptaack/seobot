package com.ptaack.seobot.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Period {
	private static final int ms = 1000;
	
	public static int parsePeriod(String period) {
		StringBuilder t = new StringBuilder();
		int value = 0;
		
		Pattern digitpattern = Pattern.compile("[0-9]");
		Matcher m = digitpattern.matcher(period);
		
		while (m.find()) {
			t.append(m.group());
		}
		
		int digit = Integer.parseInt(t.toString());
		
		char c = period.charAt(period.length()-1);
		switch (c) {
		case 's':
			value = 1;
			break;
		case 'm':
			value = 1 * 60;
			break;
			
		case 'h':
			value = 1 * 60 * 60;
			break;
			
		default:
			break;
		}
		
		return digit * value * ms;
	}
}