package com.ptaack.seobot.model;

public class Proxy {
	private String ip;
	private int port;
	
	public Proxy(String str) {
		String[] p = str.split(":");
		this.ip = p[0];
		this.port = Integer.parseInt(p[1]);
	}
	
	public String getIp() {
		return this.ip;
	}
	
	public int getPort() {
		return this.port;
	}
	
	public String toString() {
		return String.format("%s:%d", this.ip, this.port);
	}
}
