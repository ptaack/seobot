package com.ptaack.seobot.proxylist;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

public class AnonymousProxy {
	private final String URL	   = "http://www.proxy-list.net/anonymous-proxy-lists.shtml";
	private final String IPPATTERN = "((\\d{0,3}\\.){3}\\d{0,3}:\\d{0,4})";
	
	private HttpClient client;
	private GetMethod  method;
	
	public AnonymousProxy() {
		client = new HttpClient();
		method = new GetMethod(this.URL);
	}
	
	public String[] getList() {
		Pattern p = this.getIpPattern();
		
		List<String> resultArr = new ArrayList<String>();
		StringBuffer sb = new StringBuffer();
		
		try {
			client.executeMethod(method);
			InputStreamReader ir = new InputStreamReader(method.getResponseBodyAsStream());
			BufferedReader br = new BufferedReader(ir);
			String s;
			while ((s = br.readLine()) != null) {
				sb.append(s.concat("\n"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			method.releaseConnection();
		}
		
		Matcher m = p.matcher(sb.toString());
		
		while (m.find()) {
			resultArr.add(m.group());
		}
		
		return Arrays.copyOf(resultArr.toArray(), resultArr.toArray().length, String[].class);
	}
	
	public Pattern getIpPattern() {
		return Pattern.compile(this.IPPATTERN);
	}
}
