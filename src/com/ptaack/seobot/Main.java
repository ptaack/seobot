package com.ptaack.seobot;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import com.ptaack.seobot.model.Period;
import com.ptaack.seobot.tasks.FetchTask;
import com.ptaack.seobot.tasks.SeoTask;
import com.ptaack.seobot.tasks.VisitTask;


public class Main {

	public static Connection conn;
	private static final int shutdownTime = 2;                 // shutdown hook delay time in seconds	

	public List<TimerTask> Tasks;

	public static void main(String[] args) throws SQLException {
		java.util.Timer t1 = new java.util.Timer();
		//		try {
		//			SeoBotService.FetchAnonymProxy();
		//		} catch (SQLException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//
		
		Main seo = new Main();

		for(int i = 0; i < seo.Tasks.size(); i++) {
			t1.schedule(seo.Tasks.get(i), 0, ((SeoTask) seo.Tasks.get(i)).getPeriod());
		}

		Thread runtimeHookThread = new Thread() {
			public void run() {
				shutdownHook(); 
			}
		};
		Runtime.getRuntime().addShutdownHook(runtimeHookThread);		
		try {
			while (true) {
				Thread.sleep(3000);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		Main.conn.close();
	}

	private static void shutdownHook() {
		log ("ShutdownHook started");
		long t0 = System.currentTimeMillis();
		while (true) {
			try {
				Thread.sleep (500); }
			catch (Exception e) {
				log ("Exception: "+e.toString());
				break; }
			if (System.currentTimeMillis() - t0 > shutdownTime*1000) break;
			log ("shutdown"); }
		log ("ShutdownHook completed"); }

	public Main()  {
		this.Tasks = new ArrayList<TimerTask>();
		this.parseConfig();
	}

	public static void log (String msg) {
		System.out.println (getTimeStamp()+" "+msg); }

	private static String getTimeStamp() {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return f.format(new Date()); }

	static {
		SeoBotDatabase.init();
	}

	private void parseConfig() {
		try {
			BufferedReader input = new BufferedReader(new FileReader(Configuration.CFGNAME));
			String line;
			while ((line = input.readLine()) != null) {
				if (line.charAt(0) == '\u0023')
					continue;
				String[] elemArr = line.split(";");
				if (elemArr[0].toLowerCase().equals("visit")) {
					this.Tasks.add(new VisitTask(elemArr[1], elemArr[2]));
				}
				else if (elemArr[0].toLowerCase().equals("fetchproxy")) {
					this.Tasks.add(new FetchTask(elemArr[1]));
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Could not found configuration file");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
