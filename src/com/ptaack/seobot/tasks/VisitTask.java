package com.ptaack.seobot.tasks;

import com.ptaack.seobot.Main;
import com.ptaack.seobot.SeoBotMethods;
import com.ptaack.seobot.model.Period;

public class VisitTask extends SeoTask  {
	private String sitename;

	public VisitTask(String sitename, String timeinterval) {
		this.sitename = sitename;
		super.period = Period.parsePeriod(timeinterval);
	}

	@Override
	public void run() {
		Main.log("Start visiting the site: " + this.sitename);
		SeoBotMethods.fakeVisit(this.sitename);
		Main.log("Stop visiting the site");
	}
}
