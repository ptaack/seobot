package com.ptaack.seobot.tasks;

import com.ptaack.seobot.Main;
import com.ptaack.seobot.SeoBotMethods;
import com.ptaack.seobot.model.Period;

public class FetchTask extends SeoTask {

	public FetchTask(String timeinterval) {
		super.period = Period.parsePeriod(timeinterval);
	}
	@Override
	public void run() {
		Main.log("Fetch start");
		SeoBotMethods.fetchAnonymProxy();
		Main.log("Fetch stop");
	}
}
